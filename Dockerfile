FROM registry.gitlab.com/base-images/apache:2.4

ENV PHP_MAJOR_VERSION=8.2
ENV PHP_VERSION="${PHP_MAJOR_VERSION}.*"
ENV CA_CERTIFICATES_VERSION=20*

ADD --chmod=0644 https://packages.sury.org/php/apt.gpg /etc/apt/trusted.gpg.d/php.gpg

SHELL ["/bin/bash", "-x", "-e", "-u", "-o", "pipefail", "-c"]

RUN apt-get update \
 && apt-get install -y --no-install-recommends ca-certificates="${CA_CERTIFICATES_VERSION}" \
 && apt-get update \
 && apt-get install -y --no-install-recommends \
    "libapache2-mod-php${PHP_MAJOR_VERSION}=${PHP_VERSION}" \
    "php${PHP_MAJOR_VERSION}-curl=${PHP_VERSION}" \
    "php${PHP_MAJOR_VERSION}-gd=${PHP_VERSION}" \
    "php${PHP_MAJOR_VERSION}-mbstring=${PHP_VERSION}" \
    "php${PHP_MAJOR_VERSION}-mysql=${PHP_VERSION}" \
    "php${PHP_MAJOR_VERSION}-xml=${PHP_VERSION}" \
 && apt-get autoremove \
 && apt-get clean \
 && rm -rf /tmp/* \
 && rm -rf /var/lib/apt/lists/*
